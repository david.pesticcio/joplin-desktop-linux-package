# Joplin Desktop Linux package


## Description
[Joplin](https://joplinapp.org/) is a great FLOSS multiplatform notes app, this project aims to provide a ready to install package for Debian and Arch distributions.


## Motivation
Officially Joplin provides an AppImage for Linux distributions only. While it works, some users may not want to use an AppImage, or simply want an easier way to keep it up-to-date, like installing it from a repository that provides the latest version when updating the system packages.


## Debian Build
The binaries are built using the latest Joplin source code, and it is based on the [joplin-deb](https://github.com/cvandesande/joplin-deb) Dockerfile, with a few changes:

1. It uses latest Debian stable for the node Docker image;
2. It fixes the icon used in the dektop file;
3. Other small fixes/improvements to the desktop file: correct StartupWMClass, MimeType and Category;
4. Copy the correct LICENSE file;
5. The artifact is then published to the Releases page.

All of the above can be verified in the project's [.gitlab-ci.yml](./.gitlab-ci.yml) file.


## ArchLinux Build
Electron apps are basically self-contained, they usually do not require any extra dependecies besides what is normally needed to run a web browser and should already be installed.

In order to create the ArchLinux PKGBUILD, [debtap](https://github.com/helixarch/debtap) was used. Since the dekstop file and icon were already fixed in the deb package, the only change is the dependences in the [PKGBUILD](./arch/PKGBUILD) since `pacman` does not support alternative dependencies. The most common dependecy was chosen, and it should work in all GTK based desktop environments (Gnome, XFCe, etc) at least. Please report if the package did not work in your setup, opening an issue and adding the relevant details.


## Roadmap
- [x] Build an publish a Debian package (compatible with latest Debian and latest Ubuntu LTS and up)
- [x] Build and publish an Arch package
- [x] Include checksums in the Release page
- [ ] Publish deb package to a repository

## Download and install
The latest (and previous) version can be downloaded from the [Releases](https://gitlab.com/LFd3v/joplin-desktop-linux-package/-/releases) page, just download the file that works for your distro: .deb for Debian/Ubuntu and derivatives, or .pkg.tar.zst for Arch and derivatives. If you do not have a GUI app that handles it, then use the package manager from the command line to install.

The deb package may be pushed to a repo in the future, and the instructions will be added here.


## Contributing
Contributions and PR are welcome!

If you have tested the package with your distro and it worked or not, feel free to report it in the issues tab.

Also, if you know a way to optimize the build, plesase drop a line.


### Authors and acknowledgment
Joplin - https://github.com/laurent22/joplin

Joplin deb - https://github.com/cvandesande/joplin-deb

Raul Dipeas - https://github.com/rauldipeas/apt-repository


### License
AGPL 3.0 or later (as Joplin itself)

<br/><small>

### Disclaimer

Please be aware that by installing the software, you accept full responsibility for any consequences that may arise. There is no warranty or guarantee provided with this software, whether expressed or implied. The Joplin developer and I do not assume any liability for damages, data loss, or other issues resulting from the installation or use of the software. Although I have tested it on my setup it is crucial to exercise caution and understand that you are solely responsible for your actions.
</small>
